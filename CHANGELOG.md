# Sentry Changelog

## v1.1.0-kt
### Additions:

---

#### General:
* `+ CHANGELOG.md`

#### Commands:
* `+ '/sentry file' commands`
  * `/sentry file delete <ID>`
  * `/sentry file list <Player>`
  * `/sentry file addinfo <Player> <Info-MSG>`

#### Assets:
* `+ FileEntry data class`

#### Database:
* `+ fileData Table` # Information about file entries

## v1.0.0-kt
### Additions:

---

#### General:

* `+ Licensed under GNU AGPLv3`
* `+ Switched codebase to Kotlin` 
* `+ Switched from Bungeecord to Spigot Plugin`

#### Commands:

* `+ /sentry commands`
  * `+ /sentry reload` # Reloads the config-file
  * `+ /sentry setjail` # Set the jail position to the current player position
* `+ /ban command`
* `+ /tempban command`
* `+ /unban command`
* `+ /kick comamnd`
* `+ /mute command`
* `+ /unmute command`
* `+ /jail command`
* `+ /unjail command`
* `+ /jailstatus command` # Shows information about jail

#### Assets:
* `+ JailHandler - auto unjails players, after time has passed`
* `+ AFKHandler - sets players to afk, when gone for (default: 1min)`

#### Database:
* `+ banList Table` # Banned players
* `+ muteList Table` # Muted players
* `+ jailList Table` # Jailed Players
* `+ jailData Table` # Jail-Position Data

### Removals:

---

#### General:

* `- Bungeecord Classes`
* `- Java Codebase`