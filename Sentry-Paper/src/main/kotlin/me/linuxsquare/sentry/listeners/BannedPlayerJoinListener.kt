package me.linuxsquare.sentry.listeners

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerLoginEvent
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class BannedPlayerJoinListener(sentry: Sentry): Listener {

    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    @EventHandler
    fun onBannedPlayerJoin(e: PlayerLoginEvent) {
        val sentryplayer = Sentry.getSentryOfflinePlayer(e.player.uniqueId)
        if(sentryplayer.isBanned) {
            if(sentryplayer.getDuration().equals("PERMANENT", true)) {
                e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text("${Sentry.PREFIX}\n" +
                        "§cYou have been banned from this network\n" +
                        "§6From: §c${sentryplayer.getInitiator()}\n" +
                        "§6Reason: §c${sentryplayer.getReason()}\n" +
                        "§6Duration: §cPERMANENT"))
            } else {
                val ends: LocalDateTime = LocalDateTime.parse(sentryplayer.getDuration())
                val now: LocalDateTime = LocalDateTime.now()

                if(now.isEqual(ends) || now.isAfter(ends)) {
                    sentryplayer.unbanPlayer()
                    sentryplayer.addFileEntry(FileEntry(
                        sentryplayer.uniqueId,
                        LocalDate.now().toString(),
                        FileEntryType.UNBAN,
                        "${sentryplayer.name} unbanned")
                    )
                    return
                }
                e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text("${Sentry.PREFIX}\n" +
                        "§cYou have been banned from this network\n" +
                        "§6From: §c${sentryplayer.getInitiator()}\n" +
                        "§6Reason: §c${sentryplayer.getReason()}\n" +
                        "§6Duration: §c${ends.format(DateTimeFormatter.ofPattern(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")))} ${sentry.getConfigModel().getConf().getString("Settings.timezone")}"))
            }
        }
    }
}