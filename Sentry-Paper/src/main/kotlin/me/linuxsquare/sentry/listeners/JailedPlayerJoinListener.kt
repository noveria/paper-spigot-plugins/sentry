package me.linuxsquare.sentry.listeners

import me.linuxsquare.sentry.Sentry
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class JailedPlayerJoinListener(sentry: Sentry): Listener {

    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    @EventHandler
    fun onJailedPlayerJoinListener(e: PlayerJoinEvent) {
        val sp = Sentry.getSentryOfflinePlayer(e.player.uniqueId)

        if(sp.isJailed()) {
            if(!sp.teleportedToJail()) {
                if(!(sentry.getConfigModel().getConf().getBoolean("Settings.BungeeEnabled") xor sentry.getConfigModel().getConf().getBoolean("Settings.BungeeJailServer"))) {
                    if(sp.isOnline) {
                        val online = sp.player!!
                        if(sentry.getDatabaseModel().getJail() != null) {
                            val rs = sentry.getDatabaseModel().getJail()!!
                            val jailLoc = Location(Bukkit.getWorld(rs.getString("world")), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"))

                            online.teleport(jailLoc)
                            sp.updateJailBool()
                        }
                    }
                }
            }
        }
    }

}