package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class JailCommand(sentry: Sentry): CommandExecutor {

    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(!sender.hasPermission("sentry.jail")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            0,1 -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /jail <user> <amount>:<type> <reason>")
                return true
            }
            2 -> {
                jailPlayer(args[0], sender, args[1])
            }
            else -> {
                val sb = StringBuilder()

                var i = 2
                while(i < args.size) {
                    sb.append(args[i])
                    if(i == args.size - 1) {
                        sb.append("!")
                    } else {
                        sb.append(" ")
                    }
                    i++
                }
                jailPlayer(args[0], sender, args[1], sb.toString())
            }
        }
        return false
    }

    private fun jailPlayer(playername: String, initiator: CommandSender, duration: String, reason: String = "<!-- Insert Default Reason here -->") {
        val uuid: String? = sentry.getUniqueID(playername)
        if(uuid == null) {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cdoes not exist!")
            return
        }
        val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

        if(target.name.equals(initiator.name)) {
            initiator.sendMessage("${Sentry.PREFIX}§cYou cannot jail yourself!")
            return
        }

        val now: LocalDateTime = LocalDateTime.now()
        var new: LocalDateTime = now
        val durationArr = duration.split(":")
        when(durationArr[1]) {
            "m" -> {
                new = now.plusMinutes(durationArr[0].toLong())
            }
            "h" -> {
                new = now.plusHours(durationArr[0].toLong())
            }
            "d" -> {
                new = now.plusDays(durationArr[0].toLong())
            }
            "w" -> {
                new = now.plusWeeks(durationArr[0].toLong())
            }
            "M" -> {
                new = now.plusMonths(durationArr[0].toLong())
            }
            "y" -> {
                new = now.plusYears(durationArr[0].toLong())
            }
        }

        new = new.minusSeconds(Integer.toUnsignedLong(new.second))
        new = new.minusNanos(Integer.toUnsignedLong(new.nano))

        if(!target.isJailed()) {
            if(sentry.getDatabaseModel().jailCreated()) {
                Bukkit.getServer().broadcast(Component.text("${Sentry.PREFIX}§e${playername} §chas been jailed ${duration.replace(":", "")} by §e${initiator.name} §cfor: §e${reason}"))
                target.jailPlayer(initiator.name, reason, new)
                if(sentry.getConfigModel().getConf().getBoolean("Settings.BungeeEnabled")) {
                    val result = sentry.callBungeeGET("players/${uuid}")
                    if(!result.isNullOrEmpty()) {
                        if(!result["server"].toString().equals(sentry.getConfigModel().getConf().getString("Settings.BungeeJailServerName"), true)) {
                            sentry.callBungeePOST("players/connect/${uuid}", hashMapOf(Pair("server", sentry.getConfigModel().getConf().getString("Settings.BungeeJailServerName"))))
                        }
                    }
                }
                if(target.isOnline) {
                    val online = target.player!!
                    if(sentry.getDatabaseModel().getJail() != null) {
                        val rs = sentry.getDatabaseModel().getJail()!!
                        val jailLoc = Location(Bukkit.getWorld(rs.getString("world")), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"))

                        online.teleport(jailLoc)
                        target.updateJailBool()
                    }
                }
                target.addFileEntry(
                    FileEntry(
                        target.uniqueId,
                        LocalDate.now().toString(),
                        FileEntryType.JAIL,
                        reason, duration = duration.replace(":", ""))
                )
            } else {
                initiator.sendMessage("${Sentry.PREFIX}§cJail location has not been set. Please set with /sentry setjail!")
            }
        } else {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §chas been already jailed!")
        }
    }
}