package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.time.LocalDate
import java.util.*

class KickCommand(sentry: Sentry): CommandExecutor {

    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if(!sender.hasPermission("sentry.kick")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            0 -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /kick <user> <reason>")
                return true
            }
            1 -> {
                kickPlayer(args[0], sender)
            }
            else -> {
                val sb = StringBuilder()

                var i = 1
                while(i < args.size) {
                    sb.append(args[i])
                    if(i == args.size - 1) {
                        sb.append("!")
                    } else {
                        sb.append(" ")
                    }
                    i++
                }
                kickPlayer(args[0], sender, sb.toString())
            }
        }
        return false
    }

    private fun kickPlayer(playername: String, initiator: CommandSender, reason: String = "The Kick-Hammer has spoken!") {
        val uuid: String? = sentry.getUniqueID(playername)
        if(uuid == null) {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cdoes not exist!")
            return
        }

        val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

        if(target.name.equals(initiator.name)) {
            initiator.sendMessage("${Sentry.PREFIX}§cYou cannot kick yourself!")
            return
        }

        val messageComponent = Component.text("${Sentry.PREFIX}\n" +
                "§cYou have been kicked from this network\n" +
                "§6From: §c${initiator.name}\n" +
                "§6Reason: §c${reason}")

        if(sentry.getConfigModel().getConf().getBoolean("Settings.BungeeEnabled")) {
            val result = sentry.callBungeeGET("players/${uuid}")
            if(result.isNullOrEmpty()) {
                initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cis currently not online!")
                return
            }
            sentry.callBungeePOST("players/disconnect/${uuid}", hashMapOf(Pair("message", messageComponent.content())))

        } else {
            if(!target.isOnline) {
                initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cis currently not online!")
                return
            }

            target.player!!.kick(messageComponent)
        }

        Bukkit.getServer().broadcast(Component.text("${Sentry.PREFIX}§e${playername} §chas been kicked by §e${initiator.name} §cfor: §e${reason}"))
        sentry.getDatabaseModel().addFileEntry(FileEntry(
            target.uniqueId,
            LocalDate.now().toString(),
            FileEntryType.KICK,
            reason)
        )
    }
}