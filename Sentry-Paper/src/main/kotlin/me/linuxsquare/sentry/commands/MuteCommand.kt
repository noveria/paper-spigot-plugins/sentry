package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class MuteCommand(sentry: Sentry): CommandExecutor {

    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(!sender.hasPermission("sentry.mute")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            0,1 -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /mute <user> <amount>:<type> <reason>")
                return true
            }
            2 -> {
                mutePlayer(args[0], sender, args[1])
            }
            else -> {
                val sb = StringBuilder()

                var i = 2
                while(i < args.size) {
                    sb.append(args[i])
                    if(i == args.size - 1) {
                        sb.append("!")
                    } else {
                        sb.append(" ")
                    }
                    i++
                }
                mutePlayer(args[0], sender, args[1], sb.toString())
            }
        }

        return false
    }

    private fun mutePlayer(playername: String, initiator: CommandSender, duration: String, reason: String = "Your mouth has been duct-taped") {
        val uuid: String? = sentry.getUniqueID(playername)
        if(uuid == null) {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cdoes not exist!")
            return
        }
        val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

        if(target.name.equals(initiator.name)) {
            initiator.sendMessage("${Sentry.PREFIX}§cYou cannot mute yourself!")
            return
        }

        val now: LocalDateTime = LocalDateTime.now()
        var new: LocalDateTime = now
        val durationArr = duration.split(":")
        when(durationArr[1]) {
            "s" -> {
                new = now.plusSeconds(durationArr[0].toLong())
            }
            "m" -> {
                new = now.plusMinutes(durationArr[0].toLong())
            }
            "h" -> {
                new = now.plusHours(durationArr[0].toLong())
            }
            "d" -> {
                new = now.plusDays(durationArr[0].toLong())
            }
            "w" -> {
                new = now.plusWeeks(durationArr[0].toLong())
            }
            "M" -> {
                new = now.plusMonths(durationArr[0].toLong())
            }
            "y" -> {
                new = now.plusYears(durationArr[0].toLong())
            }
        }

        if(!target.isMuted()) {
            Bukkit.getServer().broadcast(Component.text("${Sentry.PREFIX}§e${playername} §chas been muted ${duration.replace(":", "")} by §e${initiator.name} §cfor: §e${reason}"))
            target.mutePlayer(initiator.name, reason, new)
            target.addFileEntry(
                FileEntry(
                target.uniqueId,
                LocalDate.now().toString(),
                FileEntryType.MUTE,
                reason, duration = duration.replace(":", ""))
            )
        } else {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §chas been already muted!")
        }
    }
}