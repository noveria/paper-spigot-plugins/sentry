package me.linuxsquare.sentry.models

import java.util.*

data class FileEntry(val uuid: UUID, val date: String, val fileEntryType: FileEntryType, val message: String, val id: Int = 0, val duration: String? = null)

enum class FileEntryType(val type : String) {
    INFO("INFO"),
    MUTE("MUTE"),
    JAIL("JAIL"),
    KICK("KICK"),
    TEMPBAN("TEMPBAN"),
    BAN("BAN"),
    UNBAN("UNBAN"),
    UNJAIL("UNJAIL"),
    UNMUTE("UNMUTE")
}
