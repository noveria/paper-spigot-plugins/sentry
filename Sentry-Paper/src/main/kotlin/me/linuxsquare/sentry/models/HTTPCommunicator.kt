package me.linuxsquare.sentry.models

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.text.StringBuilder

class HTTPCommunicator {

    companion object {
        private const val USER_AGENT: String = "Mozilla/5.0"
        private const val GET_URL: String = "https://api.mojang.com/users/profiles/minecraft/"
    }

    private var retMap: HashMap<String, Any> = HashMap()

    fun HTTPGet(playername: String): String? {
        val obj = URL("$GET_URL$playername")
        val httpURLConnection: HttpURLConnection = obj.openConnection() as HttpURLConnection
        httpURLConnection.requestMethod = "GET"
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT)
        val responseCode: Int = httpURLConnection.responseCode
        if(responseCode == HttpURLConnection.HTTP_OK) run {
            val input = BufferedReader(InputStreamReader(httpURLConnection.inputStream))
            val response = StringBuffer()

            response.append(input.readLine())
            input.close()

            val mapType = object : TypeToken<HashMap<String, Any>>() {}.type
            retMap = Gson().fromJson(response.toString(), mapType)

            return insertDashUUID(retMap.get("id").toString())
        }
        return null
    }

    private fun insertDashUUID(uuid: String): String {
        var sb: StringBuilder = StringBuilder(uuid)
        sb.insert(8,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(13,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(18,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(23, "-")

        return sb.toString()
    }

}