package me.linuxsquare.sentry.utils

import me.linuxsquare.sentry.models.FileEntry
import org.bukkit.OfflinePlayer
import java.time.LocalDateTime

interface ISentryPlayer: OfflinePlayer {

    /**
     * Returns, if the target Player is currently banned
     */
    override fun isBanned(): Boolean

    /**
     * Handles the permanent ban of the target player
     */
    fun banPlayer(initiator: String, reason: String)

    /**
     * Handles the temporary ban of the target player
     */
    fun banPlayer(initiator: String, reason: String, ends: LocalDateTime)

    /**
     * Removes the permanent or temporary ban of the target player
     */
    fun unbanPlayer()

    /**
     * Checks if the target player has been already muted
     */
    fun isMuted(): Boolean

    /**
     * Muted the target player only temporary
     */
    fun mutePlayer(initiator: String, reason: String, ends: LocalDateTime)

    /**
     * Unmutes the target player
     */
    fun unmutePlayer()

    /**
     * Checks if the target player has been already jailed
     */
    fun isJailed(): Boolean

    /**
     * Jails the target player only temporary
     */
    fun jailPlayer(initiator: String, reason: String, ends: LocalDateTime)

    /**
     * Unjails the target player
     */
    fun unjailPlayer()

    /**
     * Checks, if the player has been teleported to jail, when isJailed == true
     */
    fun teleportedToJail(): Boolean

    /**
     * Updates the jailed bool, when isJailed == true && teleportedToJail == true
     */
    fun updateJailBool()

    /**
     * Returns the initiating person, who's responsible for the punishment
     */
    fun getInitiator(): String?

    /**
     * Returns the reason of the punishment
     */
    fun getReason(): String?

    /**
     * Returns the duration of the punishment
     */
    fun getDuration(): String?

    /**
     * Updates the duration, if the jailed player is offline or afk, while isJailed == true
     */
    fun updateDuration(amount: Long)

    fun addFileEntry(fileEntry: FileEntry): Boolean

    fun getFileEntries(): MutableList<FileEntry>
}