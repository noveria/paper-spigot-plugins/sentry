package me.linuxsquare.sentry.utils

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit
import org.bukkit.scheduler.BukkitTask
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class JailHandler(sentry: Sentry) {

    private val sentry: Sentry
    private lateinit var task: BukkitTask
    private lateinit var chattask: BukkitTask

    init {
        this.sentry = sentry
    }

    fun autoUnjail() {
        val jailedPlayers = Sentry.getJailedPlayers()
        if(jailedPlayers.size > 0) {
            for(prisoner in jailedPlayers) {
                var now = LocalDateTime.now()
                val releaseTime = LocalDateTime.parse(prisoner.getDuration())

                now = now.minusNanos(Integer.toUnsignedLong(now.nano))
                now = now.minusSeconds(Integer.toUnsignedLong(now.second))

                if(!(sentry.getConfigModel().getConf().getBoolean("Settings.BungeeEnabled") xor sentry.getConfigModel().getConf().getBoolean("Settings.BungeeJailServer"))) {
                    if(!prisoner.isOnline && !sentry.getConfigModel().getConf().getBoolean("Settings.jailtimeDecreaseWhenOffline")) {
                        prisoner.updateDuration(1)
                    } else {
                        if(Sentry.getSentryPlayer(prisoner.uniqueId).isAFK() && !sentry.getConfigModel().getConf().getBoolean("Settings.jailtimeDecreaseWhenAFK")) {
                            prisoner.updateDuration(1)
                        }
                    }

                    if(prisoner.isOnline) {
                        val online = prisoner.player!!
                        val onlineSentry = Sentry.getSentryPlayer(online.uniqueId)

                        if(now.isEqual(releaseTime) || now.isAfter(releaseTime)) {
                            onlineSentry.unjailPlayer()
                            onlineSentry.addFileEntry(FileEntry(
                                onlineSentry.uniqueId,
                                LocalDate.now().toString(),
                                FileEntryType.UNJAIL,
                                "${onlineSentry.name} unjailed")
                            )
                            Bukkit.getScheduler().runTask(sentry, Runnable {
                                online.teleport(online.world.spawnLocation)
                                online.sendMessage("${Sentry.PREFIX}§aYou have been freed from jail. Now behave!")
                            })
                        }
                    }
                }
            }
        }
    }

    fun sendActionbar() {
        val jailedPlayers = Sentry.getJailedPlayers()
        if(jailedPlayers.size > 0) {
            for(prisoner in jailedPlayers) {
                if(prisoner.isOnline) {
                    val online = prisoner.player!!
                    online.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent("${Sentry.PREFIX}§4You have been imprisoned until §6§l${
                        LocalDateTime.parse(prisoner.getDuration()).format(
                            DateTimeFormatter.ofPattern(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")))} ${sentry.getConfigModel().getConf().getString("Settings.timezone")}" +
                            " §4Type §6§l/jailstatus §4for more information!"))
                }
            }
        }
    }

    fun start() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(sentry, Runnable {
            autoUnjail()
        }, 0, 20*60)
        chattask = Bukkit.getScheduler().runTaskTimerAsynchronously(sentry, Runnable {
            sendActionbar()
        }, 0, 20)
    }

    fun stop() {
        Bukkit.getScheduler().cancelTask(task.taskId)
        Bukkit.getScheduler().cancelTask(chattask.taskId)
    }

}
