package me.linuxsquare.sentry.utils

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import org.bukkit.*
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.profile.PlayerProfile
import java.time.LocalDateTime
import java.util.*

open class SentryOfflinePlayer(id: UUID): ISentryPlayer {

    private val offlinePlayer: OfflinePlayer
    protected val sentry: Sentry

    init {
        this.offlinePlayer = Bukkit.getOfflinePlayer(id)
        this.sentry = Bukkit.getPluginManager().getPlugin("Sentry") as Sentry
    }

    override fun isOp(): Boolean {
        return offlinePlayer.isOp
    }

    override fun setOp(p0: Boolean) {
        offlinePlayer.isOp = p0
    }

    override fun getName(): String? {
        return offlinePlayer.name
    }

    override fun getUniqueId(): UUID {
        return offlinePlayer.uniqueId
    }

    override fun serialize(): MutableMap<String, Any> {
        return offlinePlayer.serialize()
    }

    override fun isOnline(): Boolean {
        return offlinePlayer.isOnline
    }

    @Deprecated("'PlayerProfile' is deprecated. Deprecated in Java")
    override fun getPlayerProfile(): PlayerProfile {
        return offlinePlayer.playerProfile
    }

    override fun isBanned(): Boolean {
        return sentry.getDatabaseModel().isBanned(offlinePlayer)
    }

    override fun banPlayer(initiator: String, reason: String) {
        sentry.getDatabaseModel().banPlayer(offlinePlayer, initiator, reason)
    }

    override fun banPlayer(initiator: String, reason: String, ends: LocalDateTime) {
        sentry.getDatabaseModel().banPlayer(offlinePlayer, initiator, reason, ends.toString())
    }

    override fun unbanPlayer() {
        sentry.getDatabaseModel().unbanPlayer(offlinePlayer)
    }

    override fun isMuted(): Boolean {
        return sentry.getDatabaseModel().isMuted(offlinePlayer)
    }

    override fun mutePlayer(initiator: String, reason: String, ends: LocalDateTime) {
        sentry.getDatabaseModel().mutePlayer(offlinePlayer, initiator, reason, ends)
    }

    override fun unmutePlayer() {
        sentry.getDatabaseModel().unmutePlayer(offlinePlayer)
    }

    override fun isJailed(): Boolean {
        return sentry.getDatabaseModel().isJailed(offlinePlayer)
    }

    override fun jailPlayer(initiator: String, reason: String, ends: LocalDateTime) {
        sentry.getDatabaseModel().jailPlayer(offlinePlayer, initiator, reason, ends)
    }

    override fun unjailPlayer() {
        sentry.getDatabaseModel().unjailPlayer(offlinePlayer)
    }

    override fun teleportedToJail(): Boolean {
        return sentry.getDatabaseModel().hasBeenTeleportedToJail(offlinePlayer)
    }

    override fun updateJailBool() {
        sentry.getDatabaseModel().updateJailTeleportBool(offlinePlayer)
    }

    override fun getInitiator(): String? {
        if(isBanned) {
            return sentry.getDatabaseModel().getBanInitiator(offlinePlayer)
        }
        if(isJailed()) {
            return sentry.getDatabaseModel().getJailInitiator(offlinePlayer)
        }
        if(isMuted()) {
            return sentry.getDatabaseModel().getMuteInitiator(offlinePlayer)
        }

        return null
    }

    override fun getReason(): String? {
        if(isBanned) {
            return sentry.getDatabaseModel().getBanReason(offlinePlayer)
        }
        if(isJailed()) {
            return sentry.getDatabaseModel().getJailReason(offlinePlayer)
        }
        if(isMuted()) {
            return sentry.getDatabaseModel().getMuteReason(offlinePlayer)
        }
        return null
    }

    override fun getDuration(): String? {
        if(isBanned) {
            return sentry.getDatabaseModel().getBanDuration(offlinePlayer)
        }
        if(isJailed()) {
            return sentry.getDatabaseModel().getJailDuration(offlinePlayer)
        }
        if(isMuted()) {
            return sentry.getDatabaseModel().getMuteDuration(offlinePlayer)
        }
        return null
    }

    override fun updateDuration(amount: Long) {
        if(isJailed()) {
            val oldDuration: LocalDateTime = LocalDateTime.parse(sentry.getDatabaseModel().getJailDuration(offlinePlayer))
            val newDuration: LocalDateTime = oldDuration.plusMinutes(amount)
            sentry.getDatabaseModel().updateJailDuration(offlinePlayer, newDuration)
        }
    }

    override fun addFileEntry(fileEntry: FileEntry): Boolean {
        return sentry.getDatabaseModel().addFileEntry(fileEntry)
    }

    override fun getFileEntries(): MutableList<FileEntry> {
        return sentry.getDatabaseModel().getFileEntries(offlinePlayer)
    }

    override fun isWhitelisted(): Boolean {
        return offlinePlayer.isWhitelisted
    }

    override fun setWhitelisted(p0: Boolean) {
        offlinePlayer.isWhitelisted = p0
    }

    override fun getPlayer(): Player? {
        return offlinePlayer.player
    }

    override fun getFirstPlayed(): Long {
        return offlinePlayer.firstPlayed
    }

    @Deprecated("'getter for lastPlayed: Long' is deprecated. Deprecated in Java")
    override fun getLastPlayed(): Long {
        return offlinePlayer.lastPlayed
    }

    override fun hasPlayedBefore(): Boolean {
        return offlinePlayer.hasPlayedBefore()
    }

    override fun getBedSpawnLocation(): Location? {
        return offlinePlayer.bedSpawnLocation
    }

    override fun getLastLogin(): Long {
        return offlinePlayer.lastLogin
    }

    override fun getLastSeen(): Long {
        return offlinePlayer.lastSeen
    }

    override fun incrementStatistic(p0: Statistic) {
        offlinePlayer.incrementStatistic(p0)
    }

    override fun incrementStatistic(p0: Statistic, p1: Int) {
        offlinePlayer.incrementStatistic(p0, p1)
    }

    override fun incrementStatistic(p0: Statistic, p1: Material) {
        offlinePlayer.incrementStatistic(p0, p1)
    }

    override fun incrementStatistic(p0: Statistic, p1: Material, p2: Int) {
        offlinePlayer.incrementStatistic(p0, p1, p2)
    }

    override fun incrementStatistic(p0: Statistic, p1: EntityType) {
        offlinePlayer.incrementStatistic(p0, p1)
    }

    override fun incrementStatistic(p0: Statistic, p1: EntityType, p2: Int) {
        offlinePlayer.incrementStatistic(p0, p1, p2)
    }

    override fun decrementStatistic(p0: Statistic) {
        offlinePlayer.decrementStatistic(p0)
    }

    override fun decrementStatistic(p0: Statistic, p1: Int) {
        offlinePlayer.decrementStatistic(p0, p1)
    }

    override fun decrementStatistic(p0: Statistic, p1: Material) {
        offlinePlayer.decrementStatistic(p0, p1)
    }

    override fun decrementStatistic(p0: Statistic, p1: Material, p2: Int) {
        offlinePlayer.decrementStatistic(p0, p1, p2)
    }

    override fun decrementStatistic(p0: Statistic, p1: EntityType) {
        offlinePlayer.decrementStatistic(p0, p1)
    }

    override fun decrementStatistic(p0: Statistic, p1: EntityType, p2: Int) {
        offlinePlayer.decrementStatistic(p0, p1, p2)
    }

    override fun setStatistic(p0: Statistic, p1: Int) {
        offlinePlayer.setStatistic(p0, p1)
    }

    override fun setStatistic(p0: Statistic, p1: Material, p2: Int) {
        offlinePlayer.setStatistic(p0, p1, p2)
    }

    override fun setStatistic(p0: Statistic, p1: EntityType, p2: Int) {
        offlinePlayer.setStatistic(p0, p1, p2)
    }

    override fun getStatistic(p0: Statistic): Int {
        return offlinePlayer.getStatistic(p0)
    }

    override fun getStatistic(p0: Statistic, p1: Material): Int {
        return offlinePlayer.getStatistic(p0, p1)
    }

    override fun getStatistic(p0: Statistic, p1: EntityType): Int {
        return offlinePlayer.getStatistic(p0, p1)
    }
}