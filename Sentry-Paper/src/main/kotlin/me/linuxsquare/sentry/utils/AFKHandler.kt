package me.linuxsquare.sentry.utils

import me.linuxsquare.sentry.Sentry
import org.bukkit.Bukkit
import org.bukkit.scheduler.BukkitTask
import java.sql.Timestamp
import java.util.*

class AFKHandler(sentry: Sentry) {

    private lateinit var task: BukkitTask
    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    companion object {

        var afkPlayers: ArrayList<UUID> = ArrayList()
        var messageSent: ArrayList<UUID> = ArrayList()

        var movingPlayer: HashMap<UUID, Long> = HashMap()
        var interactingPlayer: HashMap<UUID, Long> = HashMap()
        var chattingPlayer: HashMap<UUID, Long> = HashMap()

        fun isAFK(uuid: UUID): Boolean {
            if(afkPlayers.contains(uuid)) {
                return true
            }
            return false
        }

        fun update(uuid: UUID, hm: HashMap<UUID, Long>) {
            val ts = Timestamp(System.currentTimeMillis())
            hm.remove(uuid)
            hm[uuid] = ts.time
        }
    }

    fun start() {
        val period = sentry.getConfigModel().getConf().getInt("Settings.afkPeriod")
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(sentry, Runnable {
            for(player in Bukkit.getOnlinePlayers()) {
                val ts = Timestamp(System.currentTimeMillis())
                val ms: Long = ts.time

                if(ms.minus(chattingPlayer[player.uniqueId]!!) >= period) {
                    if(ms.minus(interactingPlayer[player.uniqueId]!!) >= period) {
                        if(ms.minus(movingPlayer[player.uniqueId]!!) >= period) {
                            // Add Player to ArrayList
                            if(!afkPlayers.contains(player.uniqueId)) {
                                afkPlayers.add(player.uniqueId)
                            }
                        } else {
                            if(afkPlayers.contains(player.uniqueId)) {
                                afkPlayers.remove(player.uniqueId)
                            }
                        }
                    } else {
                        if(afkPlayers.contains(player.uniqueId)) {
                            afkPlayers.remove(player.uniqueId)
                        }
                    }
                } else {
                    if(afkPlayers.contains(player.uniqueId)) {
                        afkPlayers.remove(player.uniqueId)
                    }
                }

                if(isAFK(player.uniqueId)) {
                    if(!messageSent.contains(player.uniqueId)) {
                        messageSent.add(player.uniqueId)
                        player.sendMessage("${Sentry.PREFIX}§eYou are now AFK")
                    }
                } else {
                    if(messageSent.contains(player.uniqueId)) {
                        messageSent.remove(player.uniqueId)
                        player.sendMessage("${Sentry.PREFIX}§eYou are no longer AFK")
                    }
                }


            }
        }, 0, 20)
    }

    fun stop() {
        Bukkit.getScheduler().cancelTask(task.taskId)
    }

}