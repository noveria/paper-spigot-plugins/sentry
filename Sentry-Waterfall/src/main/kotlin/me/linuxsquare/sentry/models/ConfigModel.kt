package me.linuxsquare.sentry.models

import com.google.common.io.ByteStreams
import me.linuxsquare.sentry.Sentry
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.config.Configuration
import net.md_5.bungee.config.ConfigurationProvider
import net.md_5.bungee.config.YamlConfiguration
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

class ConfigModel(sentry: Sentry) {

    private val Bungee: ProxyServer = ProxyServer.getInstance()
    private val sentry: Sentry

    private lateinit var file: File
    private lateinit var config: Configuration

    init {
        this.sentry = sentry
    }

    fun setup() {
        config = ConfigurationProvider.getProvider(YamlConfiguration::class.java).load(loadResource(sentry, "config.yml"))
    }

    private fun loadResource(sentry: Sentry, resource: String): File {
        val pluginFolder: File = File("${Bungee.pluginsFolder}${File.separator}Sentry")
        if(!pluginFolder.exists()) {
            pluginFolder.mkdir()
        }

        val resourceFile: File = File(pluginFolder, resource)
        if(!resourceFile.exists()) {
            resourceFile.createNewFile()
            sentry.getResourceAsStream(resource)
                .use { `in` -> FileOutputStream(resourceFile).use { out -> ByteStreams.copy(`in`, out) } }
        }
        return resourceFile
    }

    fun save() {
        ConfigurationProvider.getProvider(YamlConfiguration::class.java).save(config,file)
    }

    fun getConfig(): Configuration {
        return this.config
    }
}