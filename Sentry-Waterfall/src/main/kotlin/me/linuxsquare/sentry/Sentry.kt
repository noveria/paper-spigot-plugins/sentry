package me.linuxsquare.sentry

import me.linuxsquare.sentry.events.PlayerIsJailed
import me.linuxsquare.sentry.models.ConfigModel
import me.linuxsquare.sentry.models.DatabaseModel
import me.linuxsquare.sentry.utils.SentryPlayer
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.plugin.Plugin
import java.util.*

class Sentry : Plugin() {

    companion object {
        const val PREFIX: String = "§e[Sentry]§r "

        // Yes, those are turret quotes of Portal 2.
        // I thought, they would fit quite nicely in, if the plugin's name is "Sentry"
        private val WELCOMEMSG = arrayOf("Activated", "There you are", "Target aquired.", "Deploying", "Hello!", "Sentry mode activated")
        private val SHUTDOWNMSG = arrayOf("Shutting down", "Nap time", "Goodbye!", "Target lost", "Goodnight", "Resting")

        fun getSentryPlayer(uuid: UUID): SentryPlayer {
            return SentryPlayer(uuid)
        }
    }

    private val configModel: ConfigModel = ConfigModel(this)
    private val databaseModel: DatabaseModel = DatabaseModel(this)

    override fun onEnable() {
        logger.info(PREFIX + "§a${WELCOMEMSG.random()}")
        configModel.setup()
        databaseModel.connect()

        val pm = ProxyServer.getInstance().pluginManager
        pm.registerListener(this, PlayerIsJailed(this))
    }

    override fun onDisable() {
        databaseModel.close()
        logger.info(PREFIX + "§c${SHUTDOWNMSG.random()}")
    }

    fun getConfigModel(): ConfigModel {
        return this.configModel
    }

    fun getDatabaseModel(): DatabaseModel {
        return this.databaseModel
    }

}