package me.linuxsquare.sentry.utils

import java.lang.Runnable
import java.lang.InterruptedException

abstract class SentryRunnable {
    var thread: Thread? = null
        private set
    private var stop = false
    abstract fun run()
    fun runTaskAsync() {
        thread = Thread(Runnable {
            if (stop) return@Runnable
            this.run()
        })
        thread!!.start()
    }

    fun runTaskLater(milliseconds: Int) {
        thread = Thread(Runnable {
            try {
                Thread.sleep(milliseconds.toLong())
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            if (stop) return@Runnable
            this.run()
        })
        thread!!.start()
    }

    fun cancel() {
        stop = true
    }

    fun runRepeatingTaskAsync(milliseconds: Int) {
        thread = Thread(Runnable {
            while (true) {
                if (stop) return@Runnable
                try {
                    Thread.sleep(milliseconds.toLong())
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                this.run()
            }
        })
        thread!!.start()
    }
}