package me.linuxsquare.sentry.utils

import net.md_5.bungee.api.connection.ProxiedPlayer

interface ISentryPlayer: ProxiedPlayer {

    fun isJailed(): Boolean

}