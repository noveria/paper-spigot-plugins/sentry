package me.linuxsquare.sentry.events

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.utils.SentryPlayer
import me.linuxsquare.sentry.utils.SentryRunnable
import net.md_5.bungee.api.ProxyServer
import net.md_5.bungee.api.event.LoginEvent
import net.md_5.bungee.api.event.PostLoginEvent
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.event.EventHandler
import java.util.*

class PlayerIsJailed(sentry: Sentry): Listener {

    private val Bungee = ProxyServer.getInstance()
    private val sentry: Sentry

    init {
        this.sentry = sentry
    }

    @EventHandler
    fun loginEvent(e: PostLoginEvent) {
        val sentryPlayer = Sentry.getSentryPlayer(e.player.uniqueId)
        if(sentryPlayer.isJailed()) {
            object : SentryRunnable() {
                override fun run() {
                    if(!sentryPlayer.server.info.name.equals(sentry.getConfigModel().getConfig().getString("Settings.JailServer"), true)) {
                        val jailServer = Bungee.getServerInfo(sentry.getConfigModel().getConfig().getString("Settings.JailServer"))
                        sentryPlayer.connect(jailServer)
                    }
                }
            }.runTaskLater(1000)
        }
    }
}